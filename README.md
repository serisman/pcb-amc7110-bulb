# AMC7110 - LED Driver - Bulb v1.0 #

## Schematic ##

![Schematic.PNG](https://bytebucket.org/serisman/pcb-amc7110-bulb/raw/master/output/Schematic.PNG)

## Design Rules ##

* Clearance: 7 mil
* Track Width: 20 mil
* Via Diameter: 27 mil
* Via Drill: 13 mil
* Zone Clearance: 7 mil
* Zone Min Width: 7 mil
* Zone Thermal Antipad Clearance: 7 mil
* Zone Thermal Spoke Width: 15 mil

## PCB available on OSH Park ##

* 0.3" x 0.25" (7.65 mm x 6.40 mm)
* $0.12 each ($0.35 for 3)
* [https://oshpark.com/shared_projects/ljnLBWHt](https://oshpark.com/shared_projects/ljnLBWHt)

### PCB Front ###

![PCB-Front.png](https://bytebucket.org/serisman/pcb-amc7110-bulb/raw/master/output/PCB-Front.png)

### PCB Back ###

![PCB-Back.png](https://bytebucket.org/serisman/pcb-amc7110-bulb/raw/master/output/PCB-Back.png)