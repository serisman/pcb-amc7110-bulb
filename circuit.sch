EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:AMC71xx
LIBS:circuit-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "20 jul 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L AMC7110 U1
U 1 1 53B87208
P 6150 3950
F 0 "U1" H 6150 4250 60  0000 C CNN
F 1 "AMC7110" H 6150 3650 60  0000 C CNN
F 2 "" H 6150 3950 60  0001 C CNN
F 3 "" H 6150 3950 60  0001 C CNN
	1    6150 3950
	1    0    0    -1  
$EndComp
Connection ~ 6700 3850
Connection ~ 6700 3950
$Comp
L GND #PWR04
U 1 1 53B88460
P 5450 4000
F 0 "#PWR04" H 5450 4000 30  0001 C CNN
F 1 "GND" H 5450 3930 30  0001 C CNN
F 2 "" H 5450 4000 60  0001 C CNN
F 3 "" H 5450 4000 60  0001 C CNN
	1    5450 4000
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K1
U 1 1 53CB4783
P 4950 3950
F 0 "K1" V 4900 3950 50  0000 C CNN
F 1 "CONN_3" V 5000 3950 40  0000 C CNN
F 2 "" H 4950 3950 60  0000 C CNN
F 3 "" H 4950 3950 60  0000 C CNN
	1    4950 3950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5300 3950 5600 3950
Wire Wire Line
	5450 3950 5450 4000
$Comp
L VCC #PWR?
U 1 1 53CB47B3
P 5450 3800
F 0 "#PWR?" H 5450 3900 30  0001 C CNN
F 1 "VCC" H 5450 3900 30  0000 C CNN
F 2 "" H 5450 3800 60  0000 C CNN
F 3 "" H 5450 3800 60  0000 C CNN
	1    5450 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 4050 5350 4050
Wire Wire Line
	5350 4050 5350 4350
Wire Wire Line
	6700 3850 6700 4350
Connection ~ 6700 4050
$Comp
L VCC #PWR?
U 1 1 53CB482C
P 5550 4100
F 0 "#PWR?" H 5550 4200 30  0001 C CNN
F 1 "VCC" H 5550 4200 30  0000 C CNN
F 2 "" H 5550 4100 60  0000 C CNN
F 3 "" H 5550 4100 60  0000 C CNN
	1    5550 4100
	-1   0    0    1   
$EndComp
Wire Wire Line
	5600 4050 5550 4050
Wire Wire Line
	5550 4050 5550 4100
Connection ~ 5450 3950
Wire Wire Line
	5600 3850 5300 3850
Wire Wire Line
	5450 3800 5450 3850
Connection ~ 5450 3850
Wire Wire Line
	6700 4350 5350 4350
Text Label 6700 3850 0    60   ~ 0
LED-
$EndSCHEMATC
